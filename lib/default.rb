include Nanoc::Helpers::Blogging
include Nanoc::Helpers::HTMLEscape
include Nanoc::Helpers::LinkTo
include Nanoc::Helpers::Rendering

module PostHelper
  def get_pretty_date(post)
    attribute_to_time(post[:created_at]).strftime('%B %-d, %Y')
  end
end

FOLD = '<!-- FOLD -->'
def fold
  "\n\n[pass]\n#{FOLD}\n\n"
end

def folded(item)
  parts = item.compiled_content.partition(FOLD)
  if parts[1].empty?
    parts[0]
  else
    parts[0] + link_to("Read more", item)
  end
end

def img(filename, caption)
  %(<a href="/static/#{filename}"><img class="inline" src="/static/#{filename}" alt="#{h caption}" title="#{h caption}" /></a>)
end

include PostHelper
